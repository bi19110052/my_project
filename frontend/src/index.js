import React from 'react'
import ReactDOM from 'react-dom/client'
//import { BrowserRouter } from "react-router-dom";
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
//createRoot.render(
//    <BrowserRouter>
//      <App />
//    </BrowserRouter>,
//  document.getElementById("root")
//)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

serviceWorker.unregister()
