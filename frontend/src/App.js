
import React from 'react'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import { BrowserRouter as Router,Switch, Route, Link } from 'react-router-dom'
//import CreateStudent from './components/create-student.component'
//import EditStudent from './components/edit-student.component'
import StudentList from './components/display'
import PeopleList from './components/display-people'
//import PeopleByList from './components/display-people-by-id'
function App() {
  return (
    <div className="App">
        <Router>
    
        <header className="App-header">
          <Navbar bg="dark" variant="dark">
            <Container>
    
              <Nav className="justify-content-end">
  
                <Nav>
                  <Link to={'/display'} className="nav-link">
                    Pets list
                  </Link>
                </Nav>
                <Nav>
                  <Link to={'/display-people'} className="nav-link">
                    People list
                  </Link>
                </Nav>
              </Nav>
            </Container>
          </Navbar>
        </header>
        <Container>
          <Row>
            <Col md={12}>
              <div className="wrapper">
                <Switch>
                  <Route
                    exact
                    path="/display"
                    component={(props) => <StudentList {...props} />}
                  />
                  <Route
                    exact
                    path="/display-people"
                    component={(props) => <PeopleList {...props} />}
                  />
                
                </Switch>
              </div>
            </Col>
          </Row>
        </Container>
        </Router>
  
    </div>
  )
}
export default App
