import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
//import card from 'react-bootstrap/Card'
import PetsTableRow from './PetsTable';

export default class StudentList extends Component {
    constructor(props) {
      super(props)
      this.state = {
        pets: []
      };
    }

componentDidMount() {
    axios.get('http://localhost:3002/pets/getAll')
      .then(res => {
        this.setState({
            pets: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }
  DataTable() {
    return this.state.pets.map((res, i) => {
      return <PetsTableRow obj={res} key={i} />;
    });
  }

  render() {
    return (<div className="table-wrapper">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>gender</th>
            <th>Owner</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>

    </div>);
  }
}