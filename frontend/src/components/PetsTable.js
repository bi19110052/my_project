import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
//import CardHeader from 'react-bootstrap/esm/CardHeader';
//import Card from 'react-bootstrap/Card'
export default class PetsTableRow extends Component {
    render() {
        //const  {assets } = this.props
        return (
           <tr>
                <td>{this.props.obj.name}</td>
                <td>{this.props.obj.gender}</td>
                <td>{this.props.obj.people}</td>
                <td>
                    <Link className="edit-link" to={"/edit-student/" + this.props.obj._id}>
                        Edit
                    </Link>
                    <Button size="sm" variant="danger">Delete</Button>
                </td>
            </tr>
        
            
       
         

        );
    }
}