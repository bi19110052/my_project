import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import PeopleTableRow from './PeopleTableRow';

export default class StudentList extends Component {
    constructor(props) {
      super(props)
      this.state = {
        People: []
      };
    }

componentDidMount() {
    axios.get('http://localhost:3002/people/getAll')
      .then(res => {
        this.setState({
          People: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }
  DataTable() {
    return this.state.People.map((res, i) => {
      return <PeopleTableRow obj={res} key={i} />;
    });
  }

  render() {
    return (<div className="table-wrapper">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>friends</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>
    </div>);
  }
}