let mongoose = require('mongoose');



var peopleSchema = new mongoose.Schema({
    name: String,
    friends:String,
    //pets: [ mongoose.Schema.Types.ObjectId]
})

peopleSchema.methods.joiValidate = function(obj) {
	var Joi = require('joi');
    Joi.objectId = require('joi-objectid')(Joi)
	var schema = {
		name: Joi.types.String().required(),
		friends: Joi.types.String().required(),
        //pets: Joi.ObjectId().required(),
	
	}
	return Joi.validate(obj, schema);
}
 

//const People = mongoose.model('people', newpeople);
//module.exports= { People}
module.exports =  mongoose.model('People',peopleSchema);