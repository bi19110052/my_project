const mongoose = require('mongoose');

const petsSchema = new mongoose.Schema({
    name:String,
    gender: String,
    people : mongoose.Schema.Types.ObjectId,

});

petsSchema.methods.joiValidate = function(obj) {
	var Joi = require('joi');
    Joi.objectId = require('joi-objectid')(Joi)
	var schema = {
		name: Joi.types.String().required(),
		gender: Joi.types.String().required(),
        people: Joi.ObjectId().required(),
	
	}
	return Joi.validate(obj, schema);
}

module.exports =  mongoose.model('Pets',petsSchema);