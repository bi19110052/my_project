const express = require('express');
const router = express.Router();
const { validate } = require('express-validation');
const PetsController = require('../controller/pets');
const petsValid = require('../validation/pets');
const petsController = new PetsController();


router.post('/pets/create',  petsController.CreatePets);
router.get('/pets/getAll', petsController.GetAllPets);
router.get('/pets/byID', petsController.GetPetsById);
router.delete('/pets/delete', petsController.GetPetsDelete);
router.put('/pets/update', petsController.GetPetsUpdate);
module.exports = router;