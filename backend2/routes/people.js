const express = require('express');
const router = express.Router();
const PeopleController = require('../controller/people');
const peoplecontroller = new PeopleController();
const Peoplevalidator = require('../validation/people');
const { validate } = require('express-validation');


router.post('/people/create', peoplecontroller.CreatePeople);
router.get('/people/getAll', peoplecontroller.GetAllPeople);
router.get('/people/byID', peoplecontroller.GetById);
router.delete('/people/delete', peoplecontroller.GetDelete);
router.put('/people/update', peoplecontroller.GetUpdate);
module.exports = router;