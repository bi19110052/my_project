const PetsService = require('../serives/pets');
const petservice = new PetsService(); 

module.exports = class PetsController{
    async CreatePets(req,res, next){
        const response = await petservice.CreatePets(req.body);
        if(response){
            res.send(response);

        }else{
            res.send("error");
        }

    }
    async GetAllPets(req,res,next){
        const results = await petservice.GetAllPets();

        if(results){
            res.send(results)
        }else{
            res.send("error");
        }

    }

    async GetPetsById(req,res,next){
      const id = req.query.id;
      const results = await petservice.GetPetsById(id);
     // console.log(id);
     if(results){
        res.send(results)
    }else{
        res.send("error");
    }

    }

    async GetPetsDelete(req,res,next){
        const id = req.query.id;
        const results = await petservice.GetPetsDelete(id);
       // console.log(id);
       if(results){
          res.send(results)
         // res.send("Deleted")
      }else{
          res.send("error");
      }
  
      }

      async GetPetsUpdate(req,res,next){
        const id = req.query.id;
        const people = req.body;
        const results = await petservice.GetPetsUpdate(id, people);
       // console.log(id);
       if(results){
          res.send(results)
      }else{
          res.send("error");
      }
  
      }
}

