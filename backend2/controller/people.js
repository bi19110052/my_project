
const peopleServices = require('../serives/people');
const PeopleServices = new peopleServices();

module.exports = class PeopleController {

    async CreatePeople(req,res,next){

        //call a service
        let people = req.body;
        const peoples =await PeopleServices.createpeople(people);
        if(peoples){
            res.send(peoples);

        }else{
            res.send("error");
        }
        
    }

    async GetAllPeople(req,res,next){
        const results = await PeopleServices.GetAllPeople();

        if(results){
            res.send(results)
        }else{
            res.send("error");
        }

    }

    async GetById(req,res,next){
      const id = req.query.id;
      const results = await PeopleServices.GetById(id);
     // console.log(id);
     if(results){
        res.send(results)
    }else{
        res.send("error");
    }

    }

    async GetDelete(req,res,next){
        const id = req.query.id;
        const results = await PeopleServices.GetDelete(id);
       // console.log(id);
       if(results){
          res.send(results)
         // res.send("Deleted")
      }else{
          res.send("error");
      }
  
      }

      async GetUpdate(req,res,next){
        const id = req.query.id;
        const people = req.body;
        const results = await PeopleServices.GetUpdate(id, people);
       // console.log(id);
       if(results){
          res.send(results)
      }else{
          res.send("error");
      }
  
      }
}

