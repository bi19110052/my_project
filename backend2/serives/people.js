const PeopleModel = require('../model/people');
const mongoose = require('mongoose');
const pets = require('../model/pets');

module.exports = class peopleServices{
    async createpeople(people)
    {
        const peopleToAdd = new PeopleModel(people);
        return await peopleToAdd.save();

    }

    async GetAllPeople(){
        return await PeopleModel.find({});
      
    
    }

    async GetById(id){
        return await PeopleModel.findById(id);
       // console.log(typeof(id));
       // const id = req.query.id;
        /*const result= await PeopleModel.aggregate([
            {
                $match : {_id:mongoose.Types.ObjectId(id)}
            },
            {
                $lookup: {
                    from:'pets',
                    localField: 'pets',
                    foreignField:'_id',
                    as: 'pets'
                },
            },
            {
                $project: {
                    _id:1,
                    name:1,
                    friends:1,
                    pets: {
                 
                        name:1,
                    
                    }
                }
            }
        
       
        ]);
        return result;*/

    }

    async GetDelete(id){
        return await PeopleModel.findByIdAndDelete(id);
    }

    async GetUpdate(id, updatedbook){
        return await PeopleModel.findByIdAndUpdate(id, updatedbook, {new:true});
    }

    
}

